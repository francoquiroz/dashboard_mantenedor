<?php
namespace AppBundle\Security\Authentication\Handler;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;

class LoginSuccessHandler implements AuthenticationSuccessHandlerInterface
{
    protected
        $router,
        $security,
        $em;

    public function __construct(Router $router, AuthorizationChecker  $security,\Doctrine\ORM\EntityManager $em)
    {
        $this->router = $router;
        $this->security = $security;
        $this->em=$em;
        echo "hola";
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        echo "hola";
        // URL for redirect the user to where they were before the login process begun if you want.
        // $referer_url = $request->headers->get('referer');

        // Default target for unknown roles. Everyone else go there.
        $url = 'usuarios';

        if($this->security->isGranted('ROLE_LIDER')){
            $url = 'upload';
        }

        $response = new RedirectResponse($this->router->generate($url));

        return $response;
    }
}