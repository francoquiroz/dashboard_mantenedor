<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class LoginController extends Controller
{
	 /**
     * @Route("/", name="homepage")
     * @Route("/login", name="login")
     */
     public function loginAction(Request $request)
    {
        $username = $request->get('_username');
        $password = $request->get('_password');

        if($username=="optimal" && $password=="optimal")
        {
            return $this->render('default/contrato.html.twig');

        }else{

            return $this->render(
                'login/login.html.twig'
            );

        }
    }

}
