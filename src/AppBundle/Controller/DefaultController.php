<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/l", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->render('login/login.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/contrato", name="contrato")
     */
    public function contratoAction(Request $request)
    {
        header("access-control-allow-origin: *");
        $request->headers->set('Content-Type', 'application/json');
        $request->headers->set('Access-Control-Allow-Origin', '*');

        return $this->render('default/contrato.html.twig');
    }

    /**
     * @Route("/empresa", name="empresa")
     */
    public function empresaAction(Request $request)
    {
        header("access-control-allow-origin: *");
        $request->headers->set('Content-Type', 'application/json');
        $request->headers->set('Access-Control-Allow-Origin', '*');

        return $this->render('default/empresa.html.twig');
    }

    /**
     * @Route("/lugar", name="lugar")
     */
    public function lugarAction(Request $request)
    {
        header("access-control-allow-origin: *");
        $request->headers->set('Content-Type', 'application/json');
        $request->headers->set('Access-Control-Allow-Origin', '*');

        return $this->render('default/lugar.html.twig');
    }

    /**
     * @Route("/faena", name="faena")
     */
    public function faenaAction(Request $request)
    {
        header("access-control-allow-origin: *");
        $request->headers->set('Content-Type', 'application/json');
        $request->headers->set('Access-Control-Allow-Origin', '*');

        return $this->render('default/faena.html.twig');
    }

    /**
     * @Route("/atributoequipo", name="atributoequipo")
     */
    public function atributoequipoAction(Request $request)
    {
        header("access-control-allow-origin: *");
        $request->headers->set('Content-Type', 'application/json');
        $request->headers->set('Access-Control-Allow-Origin', '*');

        return $this->render('default/atributoequipo.html.twig');
    }

}
